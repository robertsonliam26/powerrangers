"use strict";

const sidebarTooltips = {
	"0,0": "How much money you have.",
	"1,0": "How much money you have.",
	"2,0": "How much money you have.",
	"0,1": "The current amount of generators you have.",
	"1,1": "The maximum amount of generators you can have AND the current amount of chunks on screen.",
	"2,1": "The maximum amount of chunks you can have.",
};

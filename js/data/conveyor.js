"use strict";

// Factors of 60:
// 1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30 and 60

const conveyorSpeeds = {
	conveyor: 100,
	conveyor2: 200,
	conveyor3: 300,
	conveyor4: 400,
	conveyor5: 500,
	conveyor6: 600,
	conveyor7: 1000,
	conveyor8: 1200,
	conveyor9: 1500,
	conveyor10: 2000,
};
